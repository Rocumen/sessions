const express = require(`express`);
const cartController = require(`../controller/cartController`);
const auth = require(`../auth`);

// [SECTION] Routing Component
const router = express.Router();
// Destructure from auth
const { verify, verifyAdmin } = auth;

// Add to Cart
router.post(`/add-to-cart`, verify, cartController.addToCart);

// Retrieve Products from Cart
router.get(`/viewCart`, verify, cartController.viewCart);

// Update Cart
router.put(`/updateCart`, verify, cartController.updateCart);

// Delete Product from the Cart
router.delete(`/remove-product`, verify, cartController.deleteOneItem);

// Delete Cart
router.delete(`/delete-cart`, verify, cartController.deleteCart);

router.get(`/`);

router.post(`/checkout`, verify, cartController.checkout);

router.post(`/checkBox`, verify, cartController.checkBox);

module.exports = router;
