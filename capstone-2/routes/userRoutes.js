const express = require(`express`);
const userController = require(`../controller/userController`);
const auth = require(`../auth`);

// [SECTION] Routing Component
const router = express.Router();

// Destructure from auth
const { verify, verifyAdmin } = auth;

// user registration routes
router.post(`/register`, userController.registerUser);

// checks if email exist
router.post(`/check-email`, userController.checkEmail);

// login user/admin
router.post(`/login`, userController.loginUser);

// make a user as admin
router.put(
  `/activateAdmin/:userId`,
  verify,
  verifyAdmin,
  userController.activateAdmin
);

// make a user as User
router.put(
  `/deactivateAdmin/:userId`,
  verify,
  verifyAdmin,
  userController.deactivateAdmin
);

// Retrieve all User
router.get(`/allUser`, verify, verifyAdmin, userController.getAllUser);

// Retrieve One User by id
router.get(`/profile`, verify, userController.getUser);

// Update User
router.put(`/profile`, verify, userController.updateUser);

// Reset Password
router.put(`/reset-password`, verify, userController.resetPassword);

// Retrieve user details
router.get(`/details`, verify, userController.getProfile);

// [SECTION] Export Route System
module.exports = router;
