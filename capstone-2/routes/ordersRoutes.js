const express = require(`express`);
const ordersController = require(`../controller/ordersController`);
const auth = require(`../auth`);
const router = express.Router();

const { verify, verifyAdmin } = auth;

//Retrieve Orders (User)
router.get(`/`, verify, ordersController.getUserOrders);

module.exports = router;
