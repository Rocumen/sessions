const express = require(`express`);
const productController = require(`../controller/productController`);
const auth = require(`../auth`);
const cloudinary = require("cloudinary").v2;

cloudinary.config({
  cloud_name: "dg60equpb",
  api_key: "319573981594488",
  api_secret: "V7Xy4ZbUcN2wJAspX-l4d6KhIA0",
});

// CLOUDINARY_URL=cloudinary://319573981594488:V7Xy4ZbUcN2wJAspX-l4d6KhIA0@dg60equpb

// [SECTION] Routing Component
const router = express.Router();
// Destructure from auth
const { verify, verifyAdmin } = auth;

// Create Product
router.post(`/`, verify, verifyAdmin, productController.addProduct);

// Retrieve Products
router.get(`/allProducts`, productController.getAllProducts);

// Retrieve Products by Categories
router.get(`/byCategory`, productController.getCategory);

// Retrieve All Active Products
router.get(`/activeProducts`, productController.activeProducts);

// Retrieve Single Product
router.get(`/:productId`, productController.getSingleProduct);

// Retrieve Product Stocks
router.get(`/stocks`, verify, productController.stockLeft);

// search product by name
router.post(`/searchProduct`, productController.productSearch);

// Update Product Information
router.put(`/:productId`, verify, verifyAdmin, productController.updateProduct);

// Archive a Product
router.put(
  `/archive/:productId`,
  verify,
  verifyAdmin,
  productController.archiveProduct
);

router.put(
  `/activate/:productId`,
  verify,
  verifyAdmin,
  productController.activateProduct
);
// [SECTION] Export Route System
module.exports = router;
