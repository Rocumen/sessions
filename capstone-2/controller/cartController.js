const User = require(`../models/User`);
const Product = require(`../models/Product`);
const Cart = require(`../models/Cart`);
const Orders = require(`../models/Orders`);

// Add to cart
module.exports.addToCart = async (req, res) => {
  try {
    if (req.user.isAdmin) {
      console.log(`admins cannot add to cart`);
      return res.send(false);
    }

    // Get user information
    const user = await User.findById(req.user.id);

    if (!user) {
      console.log("Please login");
      return res.send(false);
    }

    let cart = await Cart.findOne({
      userId: req.user.id,
    });

    if (!cart) {
      cart = new Cart({
        userId: req.user.id,
        items: [],
      });
    }

    let productNameToAdd = req.body.productName;
    // Convert user's input to a Number
    let newQuantity = Number(req.body.quantity);

    // Retrieve the product from your database with a case-insensitive search
    const productNamePattern = new RegExp(productNameToAdd, "i");
    const product = await Product.findOne({ productName: productNamePattern });

    if (!product) {
      // If the product doesn't exist in the database
      return res.send(`Cannot find this product: ${productNameToAdd}`);
    }

    // Calculate the total price for the new item
    const newPrice = product.price * newQuantity;

    // Find the index of the product in the cart's items array based on the product ID
    const existingProductIndex = cart.items.findIndex(
      (item) => item.productId.toString() === product._id.toString()
    );

    if (existingProductIndex >= 0) {
      // Adding quantity to an existing product in the cart
      cart.items[existingProductIndex].quantity += newQuantity;
      // cart.items[existingProductIndex].price += newPrice;
      cart.items[existingProductIndex].totalPrice += newPrice;
    } else {
      if (newQuantity > product.quantity) {
        console.log("Product Quantity Exceeds available stocks");
        return res.send("Product Quantity Exceeds available stocks");
      }

      let addToCart = {
        productId: product._id,
        productName: product.productName,
        quantity: newQuantity, // Use the newQuantity here
        price: product.price, // Set the calculated price
        totalPrice: newPrice,
      };
      cart.items.push(addToCart);
    }

    cart.totalPrice = cart.items.reduce(
      (total, item) => total + item.totalPrice,
      0
    );

    await cart.save();
    console.log("Product Added to Cart");
    return res.send(true);
  } catch (error) {
    console.log(error);

    return res.status(500).json({ message: "Internal Server Error!" });
  }
};

// View Cart
module.exports.viewCart = async (req, res) => {
  try {
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log("Cart not found");
      return res.send(false);
    }

    // Retrieve the user's name based on the userId
    const user = await User.findById(req.user.id);

    if (!user) {
      console.log("User not found");
      return res.send(false);
    }

    // Update the cart object to include the userName
    const cartWithUserName = {
      ...cart.toObject(), // Convert the cart to a plain JavaScript object
      userName: user.fullName, // Replace 'fullName' with the actual field storing the user's name
    };

    return res.send(cartWithUserName);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

// update Cart
module.exports.updateCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const productNameToUpdate = req.body.productName;
    const newQuantity = Number(req.body.quantity);

    // Find the cart for the user
    const cart = await Cart.findOne({ userId });

    if (!cart) {
      console.log(`Cart not found`);
      return res.status(404).send(false);
    }

    // Find the index of the product in the cart's items array based on the product name (case-insensitive)
    const productIndex = cart.items.findIndex((item) =>
      new RegExp(`^${productNameToUpdate}$`, "i").test(item.productName)
    );

    if (productIndex === -1) {
      console.log(`Product not found in the cart`);
      console.log(productIndex);
      return res.status(404).send(false);
    }

    // Update the quantity of the product
    cart.items[productIndex].quantity = newQuantity;

    // Save the updated cart
    await cart.save();

    console.log(`Product updated!`);
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};

// Delete Product from Cart
module.exports.deleteOneItem = async (req, res) => {
  try {
    // Get the user's cart
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log(`Cart not found`);
      return res.send(null);
    }

    const productNameToDelete = req.body.productName;

    // Find the index of the product in the cart's items array based on the product name (case-insensitive)
    const productIndexToDelete = cart.items.findIndex((item) =>
      new RegExp(`^${productNameToDelete}$`, "i").test(item.productName)
    );

    if (productIndexToDelete === -1) {
      console.log(`Product not found in the cart`);
      return res.send(false);
    }

    // Remove the entire product from the cart
    cart.items.splice(productIndexToDelete, 1);

    // Save the updated cart
    await cart.save();

    console.log(`Product removed from the cart`);
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};

// Delete Cart
module.exports.deleteCart = async (req, res) => {
  try {
    // Find and remove the user's cart
    const cart = await Cart.findOneAndDelete({ userId: req.user.id });

    if (!cart) {
      return res.send(`Cart not found`);
    }

    return res.send(`Cart deleted successfully`);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};

// Check out cart and push it to the Orders
module.exports.checkout = async (req, res) => {
  try {
    // Get the user's cart
    const cart = await Cart.findOne({ userId: req.user.id });

    if (!cart) {
      console.log(`Cart is Empty`);
      return res.send(null);
    }

    // Calculate the total cart amount and totalCartPrice
    let totalCartAmount = 0; // Initialize totalCartAmount to zero
    let totalCartPrice = 0; // Initialize totalCartPrice to zero

    // Update product stocks and create an array to track updated products
    const myProducts = [];

    for (const requestedItem of req.body.productsToCheckout) {
      // Case-insensitive search for the product within the cart's items
      const item = cart.items.find((cartItem) =>
        new RegExp(`^${requestedItem.productName}$`, "i").test(
          cartItem.productName
        )
      );

      if (!item) {
        console.log(`product not found in cart`);
        return res.send(
          `Product not found in cart: ${requestedItem.productName}`
        );
      }

      // Find the product from the database using a case-insensitive query
      const product = await Product.findOne({
        productName: new RegExp(`^${requestedItem.productName}$`, "i"),
      });

      // if (!product) {
      //   return res.send(`Product not found: ${requestedItem.productName}`);
      // }

      if (requestedItem.quantity > item.quantity) {
        return res.send(
          `Requested quantity exceeds cart quantity for ${requestedItem.productName}`
        );
      }

      if (requestedItem.quantity > product.stock) {
        return res.send(`Insufficient stock for ${requestedItem.productName}`);
      }

      // Update product stock
      product.stock -= requestedItem.quantity;
      await product.save();

      // Deduct the quantity from the cart
      item.quantity -= requestedItem.quantity;

      // Calculate the total price for the current item
      const itemPrice = product.price * requestedItem.quantity;

      // Add the updated product to the array with quantity
      myProducts.push({
        productId: product._id,
        productName: product.productName,
        updatedQuantity: requestedItem.quantity,
        price: itemPrice, // Add the price to the myProducts array
        quantity: requestedItem.quantity, // Add the quantity to the myProducts array
      });

      // Update the total cart amount and totalCartPrice
      totalCartAmount += itemPrice;
      totalCartPrice += itemPrice;
    }

    // Save the billing address to the user's details
    const user = await User.findById(req.user.id);

    if (!user) {
      console.log(`User not found`);
      return res.send(false);
    }

    user.billingAddress = req.body.billingAddress; // Update the billingAddress field

    await user.save();

    // Clear the user's cart of items with quantity <= 0
    cart.items = cart.items.filter((item) => item.quantity > 0);

    // Check if the cart is empty and delete it if it is
    if (cart.items.length === 0) {
      await Cart.findOneAndDelete({ userId: req.user.id });
    } else {
      // Save the updated cart
      await cart.save();
    }

    // Create an order object for the current checkout
    const order = {
      userId: req.user.id,
      userName: user.fullName,
      items: myProducts, // The array of updated products from the cart
      purchasedOn: new Date(),
      totalCartPrice: totalCartPrice, // Add totalCartPrice to the order
    };
    await Orders.findByIdAndUpdate(
      req.user.id, // Adjust this query to match your use case
      { $push: { activeOrders: order } },
      { new: true, upsert: true }
    );

    // Return the user's name, updated products, and total cart amount
    return res.json({
      userName: user.fullName,
      myProducts,
      totalCartAmount,
      message: `Checkout successful`,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: `Internal Server Error` });
  }
};

// Cart Checkbox
module.exports.checkBox = async (req, res) => {
  try {
    const { productName } = req.body;

    const userCart = await Cart.findOne({ userId: req.user.id });

    const existingProduct = userCart.items.find((item) => {
      item.productName === productName;
    });

    if (existingProduct) {
      console.log(existingProduct);
    } else {
      userCart.items.push({
        productName,
      });
    }

    await userCart.save();

    res.json({ message: "Product added to checkbox" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "An error occured" });
  }
};
