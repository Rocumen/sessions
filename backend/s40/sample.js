// if no user = there is no register user
// if registered = you are already registered

app.post(`/signup`, (req, res) => {
  console.log(req.body);

  //   const users = [];

  //   if (req.body.username !== `` && req.body.password !== ``) {
  //     users.push(req.body);
  //     res.send(`User ${req.body.username} successfully`);
  //   } else {
  //     res.send(`Please input BOTH username and password`);
  //   }

  const { username, password } = req.body;

  // Check if the user is already registered
  if (username !== "" && password !== "") {
    const isUserRegistered = users.some((user) => user.username === username);

    if (isUserRegistered) {
      res.send(`User ${username} is already registered.`);
    } else {
      users.push({ username, password });
      res.send(`User ${username} successfully registered.`);
    }
  } else {
    res.send(`Please input BOTH username and password.`);
  }
});

app.put(`/change-password`, (req, res) => {
  let message;

  for (let i = 0; i < users.length; i++) {
    if (req.body.username === users[i].username) {
      users[i].password = req.body.password;

      message = `User ${req.body.username}'s password has been updated.`;
      break;
    } else {
      message = `User does not exist`;
    }
  }
  res.send(message);
});
