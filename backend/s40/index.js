const express = require(`express`);

const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
// Allow your app to read data from forms
app.use(express.urlencoded({ extended: true }));
// accepts all data types

// [SECTION] Routes

// GET Method
app.get(`/`, (req, res) => {
  res.send(`Hello World!`);
});

app.get(`/hello`, (req, res) => {
  res.send(`Hello from /hello end point.`);
});

// route expects to receive a POST method at the URI "/hello"
app.post(`/hello`, (req, res) => {
  res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`);
});

// POST Route to register user
const users = [];

// app.post(`/signup`, (req, res) => {
//   console.log(req.body);

//   if (req.body.username !== `` && req.body.password !== ``) {
//     users.push(req.body);
//     res.send(`User ${req.body.username} successfully`);
//   } else {
//     res.send(`Please input BOTH username and password`);
//   }
// });
app.post(`/signup`, (req, res) => {
  const { username, password } = req.body;
  // Check if the user is already registered
  if (username !== "" && password !== "") {
    const isUserRegistered = users.some((user) => user.username === username);

    if (isUserRegistered) {
      res.send(`User ${username} is already registered.`);
    } else {
      users.push({ username, password });
      res.send(`User ${username} successfully registered.`);
    }
  } else {
    res.send(`Please input BOTH username and password.`);
  }
});

// Change password - PUT Method

app.put(`/change-password`, (req, res) => {
  let message;

  //   for (let i = 0; i < users.length; i++) {
  //     if (req.body.username === users[i].username) {
  //       users[i].password = req.body.password;

  //       message = `User ${req.body.username}'s password has been updated.`;
  //       break;
  //     } else {
  //       message = `User does not exist`;
  //     }
  //   }

  if (users.length === 0 || users === []) {
    message = "There is no registered user.";
  } else {
    for (let i = 0; i < users.length; i++) {
      if (req.body.username === users[i].username) {
        users[i].password = req.body.password;

        message = `User ${req.body.username}'s password has been updated.`;
        break;
      } else {
        message = "User does not exist.";
      }
    }
  }

  res.send(message);
});

if (require.main === module) {
  app.listen(port, () => console.log(`Server is running at port ${port}`));
}
