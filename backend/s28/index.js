// Non-Mutator methods

let countries = [`US`, `PH`, `CAN`, `SG`, `TH`, `PH`, `FR`, `DE`];

// IndexOf(); starts at the beginning

let firstIndex = countries.indexOf(`PH`);
console.log(`Result of indexOf ${firstIndex}`);

let invalidCountry = countries.indexOf(`BR`);
console.log(`Result of indexOf ${invalidCountry}`);

//lastIndexOf() = starts at the end
let lastIndex = countries.lastIndexOf(`PH`);
console.log(`Result of lastIndexOf ${lastIndex}`);

//last
let lastIndexStart = countries.lastIndexOf(`PH`, 4);
console.log(`Result of lastIndexOf ${lastIndexStart}`);

//slice() = starts in the input value

console.log(countries);

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);

//sliced in 2 and 4th
let slicedArrayB = countries.slice(2, 4);
console.log(slicedArrayB);

//if using - count backwards and slice
let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);

// toString() = puts the array visual to string visual

let stringArray = countries.toString();
console.log(stringArray);

// concat()

let taskArrayA = [`Drink HTML`, `Eat Javascript`];

let taskArrayB = [`Inhale CSS`, `Breath SASS`];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

// additional
let combinedTasks = taskArrayA.concat(`Smell Express`, `Throw React`);
console.log(combinedTasks);

// join()

let users = [`John`, `Jane`, `Joe`, `Robert`];

console.log(users.join());
console.log(users.join(``));
console.log(users.join(`x`));

// forEach()

tasks.forEach(function (task) {
  console.log(task);
});

let filteredTasks = [];

// tasks.forEach((task) => (task.length > 8 ? filteredTasks.push(task) : null));

tasks.forEach(function (task) {
  if (task.length > 8) {
    filteredTasks.push(task);
  }
});

console.log(`Result of filtered task`);
console.log(filteredTasks);

//every() = checks all values

let numbers = [1, 2, 3, 4, 5];

let allValid = numbers.every(function (number) {
  return number < 5;
});

console.log(allValid);

// some()

let someValid = numbers.some(function (number) {
  return number < 3;
});

console.log(someValid);

// filter()

let filterValid = numbers.filter(function (number) {
  return number <= 5;
});

console.log(filterValid);

// includes() = searching sa loob ng array yung value

let products = [`Mouse`, `Keyboard`, `Laptop`, `Monitor`];

let productFound1 = products.includes(`Mouse`);

console.log(products);
console.log(productFound1);

// additional example

let filteredProducts = products.filter(function (products) {
  return products.toLowerCase().includes(`a`);
});

console.log(filteredProducts);
