const Task = require(`../models/Task.js`);
// get all task
module.exports.getAllTask = () => {
  return Task.find({}).then((result) => {
    return result;
  });
};
//creating a task
module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    // req.body.bane === requestBody.name
    name: requestBody.name,
  });
  return newTask.save().then((task, error) => {
    if (error) {
      console.log(error);
    } else {
      return task;
    }
  });
};
// Delete a task
module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
    if (error) {
      console.log(error);
    } else {
      return removedTask;
    }
  });
};
// updating a task
module.exports.updateTask = (taskId, requestBody) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.error(error);
      return result.send(`Cannot find data with the provided id.`);
    }
    result.name = requestBody.name;

    return result.save().then((updatedTask, error) => {
      if (error) {
        console.log(error);
        return res.send(`There is an error while saving the update.`);
      } else {
        return updatedTask;
      }
    });
  });
};

// getting specific task
module.exports.getSpecificTask = (taskId) => {
  return Task.findById(taskId).then((result) => {
    return result;
  });
};

// Updating the status
module.exports.taskToComplete = (taskId) => {
  return Task.findById(taskId).then((result, error) => {
    if (error) {
      console.error(error);
      return result.send(`Cannot find data with the provided id.`);
    }
    result.status = "completed";

    return result.save().then((updatedStatus, error) => {
      if (error) {
        console.log(error);
        return res.send(`There is an error while saving the update.`);
      } else {
        return updatedStatus;
      }
    });
  });
};
