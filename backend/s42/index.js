const express = require(`express`);
const mongoose = require(`mongoose`);
const taskRoute = require(`./routes/taskRoute`);

// Server setup + middleware
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// Default endpoint
// localhost:4000/tasks
app.use(`/tasks`, taskRoute);

// DB Connection

mongoose.connect(
  `mongodb+srv://admin:admin123@cluster0.aw6ktpg.mongodb.net/B305-to-do?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // allows us to avoid any current or future errors while connecting to mongoDB
  }
);

// Server listening
if (require.main === module) {
  app.listen(port, () => console.log(`Server running at port:${port}`));
}

let db = mongoose.connection;
// If error occured, output in console.
db.on(`error`, console.error.bind(console, `Connection Error!`));
// If connection is successful, output in console
db.once(`open`, () => console.log(`We're connected to the cloud database.`));
