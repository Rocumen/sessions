const http = require(`http`);

const port = 3000;

const app = http.createServer((req, res) => {
  if (req.url == `/login`) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end(`You are now in the Log-in page.`);
  } else {
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.end(`Error Message`);
  }
});

app.listen(port);
console.log(`Server now accessible at localHost ${port}`);
