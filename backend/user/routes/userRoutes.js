// [SECTION ] Dependecies and Modules
const express = require(`express`);
const userController = require(`../controllers/userControllers`);
const auth = require(`../auth.js`);

// [SECTION] Routing Component
const router = express.Router();

// Destructure from auth
const { verify, verifyAdmin } = auth;

router.get(`/allUser`, userController.getAllProfile);

router.get(`/details`, userController.getProfile);

router.post(`/register`, userController.createProfile);

// [SECTION] Export Route System
module.exports = router;
