// [SECTION] Dependencies and modules

const express = require(`express`);
const mongoose = require(`mongoose`);
const cors = require(`cors`);

require("dotenv").config();

// Allows access to routes defined within our app
const userRoutes = require(`./routes/userRoutes`);

// [SECTION] Environment Setup
const port = 3000;

//[SECTION] server setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Allows all resources to access our backend application
app.use(cors());

// Database Connection
mongoose.connect(process.env.dbURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // allows us to avoid any current or future errors while connecting to mongoDB
});

// console.log(process.env.dbURL);

mongoose.connection.once(`open`, () =>
  console.log(`We're connected to the cloud database.`)
);

// [SECTION] Backend Routes
app.use(`/users`, userRoutes);

//  [SECTION] Server Gateway Response
if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port} `);
  });
}

module.exports = app;
