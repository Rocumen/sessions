// [SECTION] Dependencies and Modules
const User = require(`../models/User`);
const bcrypt = require(`bcrypt`);

const auth = require(`../auth`);

module.exports.getAllProfile = (req, res) => {
  return User.find({}).then((result) => {
    console.log(result);
    if (result === null) {
      return res.send(`No data`);
    } else {
      return res.send(result);
    }
  });
};

module.exports.getProfile = (req, res) => {
  return User.findById(req.user.id).then((result, error) => {
    if (error) {
      response.send(null);
    } else {
      result.password = "";
      res.send(result);
    }
  });
};

module.exports.createProfile = (req, res) => {
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    mobileNo: req.body.mobileNo,
    password: bcrypt.hashSync(req.body.password, 10),
  });

  return newUser
    .save()
    .then((user, error) => {
      if (error) {
        res.send(false);
      } else {
        res.send(true);
      }
    })
    .catch((err) => err);
};
