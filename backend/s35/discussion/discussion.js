// [SECTION] Comparison Query Operator

// $gt / $gte operator = greater than / greater than or equal
// $lt / $lte = less than / less than or equal
// $ne operator = not equal
// $in operator =
/*

Syntax:

db.collectionName.find({field: {$gt: value}})
db.collectionName.find({field: {$gte: value}})

db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 50 } });

db.users.find({ age: { $ne: 82 } });

db.users.find({ lastName: { $in: [] } });


*/

db.users.find({ age: { $gt: 50 } });
db.users.find({ age: { $gte: 50 } });

db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 50 } });

db.users.find({ age: { $ne: 82 } });

db.users.find({ lastName: { $in: [`Hawking`, `Doe`] } });
db.users.find({ courses: { $in: [`HTML`, `React`] } });

// [SECTION] Logical Query Operator

/*

Syntax: 
$or operator
db.users.find({$or: [{fieldA: value}, {fieldB: value}]});

$and operator
db.users.find({$and: [{fieldA: value}, {fieldB: value}]});


*/
//or
db.users.find({ $or: [{ firstName: `Neil` }, { age: 25 }] });

// or with gt
db.users.find({ $or: [{ firstName: `Neil` }, { age: { $gt: 30 } }] });

//and
db.users.find({ $and: [{ age: { $ne: 82 } }, { age: { $ne: 76 } }] });

// [Section] Field Projection

// inclusion
/*
    - Allows us to include/add specific fields only when retrieving documents.
    - The value provided is 1 to denote that the field is being included.
    - Syntax
        db.users.find({criteria},{field: 1})
    1 = true
    0 = false
*/

db.users.find(
  {
    firstName: `Jane`,
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
  }
);

db.users.find(
  {
    firstName: `Jane`,
  },
  {
    firstName: 1,
    lastName: 1,
    "contact.email": 1,
  }
);

// Exclusion

db.users.find(
  {
    firstName: `Jane`,
  },
  {
    department: 0,
    contact: 0,
  }
);

// Suppressing the ID Field

db.users.find(
  {
    firstName: `Jane`,
  },
  {
    firstName: 1,
    lastName: 1,
    contact: 1,
    _id: 0,
  }
);

// [SECTION] Evaluation Query Operator

// $regex operator

/*

Syntax: 

db.users.find({field: {$regex: `pattern`, $options: `$optionValue`}});

*/

// Case sensitive Query

db.users.find({ firstName: { $regex: `N` } });

// Case Insensitive Query

//MongoDB v4 /i -> $i

db.users.find({ firstName: { $regex: `J`, $options: `i` } });

db.users.find({ firstName: { $regex: `o`, $options: `i` } });
