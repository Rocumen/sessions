// Array Mutator Methods

let fruits = [
    `Apple`,
    `Orange`,
    `Kiwi`,
    `Dragon Fruit`
];

// Push();
// Add Element in the end of an array

console.log(fruits);

let fruitsLength = fruits.push(`Mango`);
console.log(fruitsLength);
console.log(`Array after push method`);
console.log(fruits);

// push multiple Elements

fruits.push(`Avocado`, `Guava`,);
console.log(`Array after push method`);
console.log(fruits);

function addFruit(fruit) {
    //push parameter
    fruits.push(fruit);
    console.log(fruits);
}

addFruit(`Pineapple`);

// pop() = removes element at the end of an array

fruits.pop();
fruits.pop();
console.log(fruits);

function removeFruit() {
    fruits.pop();
    console.log(fruits);
}

removeFruit();

// unshift()

// add element in the beginning of an array

fruits.unshift(`Lime`, `Banna`);
console.log(`Array after unshift method`);
console.log(fruits);

function unshiftFruit(fruit) {
    fruits.unshift(fruit);
    console.log(fruits);
};

unshiftFruit(`Calamansi`);

// shift() = remove an element in the beginning of an array

fruits.shift();
console.log(`Array after shift Method`);
console.log(fruits);

function shiftFruit() {
    fruits.shift();
    console.log(fruits);
}

shiftFruit();

// splice(); = simultaneously removes elements from a slpiced index number and adds element

// syntax: array.splice(startIndex, deleteCount, elementsToBeAddded)

fruits.splice(1, 2, `avocado`);
console.log(fruits);

function spliceFruit(index, deleteCount, fruit) {
    fruits.splice(index, deleteCount, fruit);
    console.log(fruits);
}

spliceFruit(1, 1, `Cherry`);
spliceFruit(2, 1, `Durian`);

//sort() = arranges the elements in alphanumeric order

fruits.sort();
console.log(`Array after sort method`);
console.log(fruits);

// reverse()


fruits.reverse();
console.log(`Array after sort method`);
console.log(fruits);

spliceFruit(1, 1)