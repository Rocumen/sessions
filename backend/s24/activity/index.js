// console.log("Hello World");


//Objective 1
//Add code here
//Note: function name is numberLooper

let number = Number(0);
let a = 100;

for (a; a >= number; a--) {

    if (a <= 50) {
        break;
    }

    if (a % 10 === 0 && number % 10 !== 0) {
        console.log(`the number is being skipped and continue to the next iteration of the loop`)
        continue;
    }

    if (a % 5 === 0) {
        console.log(`${a} is divisible by 5`)
    }

}




/*
//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here
*/
let string = 'supercalifragilisticexpialidocious';
let filteredString = '';

let vowels = `aeiou`

for (let i = 0; i < string.length; i++) {
    let letters = string[i];


    if (vowels.includes(letters)) {
        continue;

    } else {
        filteredString = filteredString + letters;
        console.log(filteredString)
    }

}

console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
/*
try {
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch (err) {

}*/
