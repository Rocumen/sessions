
let cellphone = {
    name: `Nokia 3210`,
    manufactureDate: 1999
}

console.log(cellphone);

function Laptop(brand, name, manufactureDate) {
    this.brand = brand,
        this.name = name,
        this.manufactureDate = manufactureDate
}

//instance = `new`

let laptop = new Laptop('acer', 'predator', 2022);
console.log(laptop)


let myLaptop = new Laptop(`apple`, `macbook`, `2022`)

console.log(myLaptop)

let car = {}

car.name = `Honda Civic`;
car[`manufactureDate`] = 2019

console.log(car);

let person = {
    name: `John`,
    talk: function () {
        console.log(`Hello ` + this.name)
    }
}

console.log(person)
console.log(`result from object methods:`);
person.talk();

//add methods 
person.walk = function () {
    console.log(this.name + `walked 25 steps forward`)
}

person.walk();

let friend = {
    firstName: `Joe`,
    Lastname: `Smith`,
    address: {
        city: `Austin`,
        country: `Texas`
    },
    emails: [`joe@mail.com`, `joesmith@mail.com`],
    introduce: function () {
        console.log(`Hello my name is ${this.firstName} ${this.Lastname}`)
    }
}

friend.introduce();
console.log(friend.emails[1])

