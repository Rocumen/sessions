const jwt = require(`jsonwebtoken`);

const secret = `eSportBookingAPI`;

// token creation

module.exports.createAccessToken = (player) => {
  const data = {
    id: player._id,
    email: player.email,
    isAdmin: player.isAdmin,
  };

  return jwt.sign(data, secret, {});
};
