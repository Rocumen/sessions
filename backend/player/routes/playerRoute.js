const express = require(`express`);

const router = express.Router();
const playerController = require(`../controllers/playerController.js`);

router.get(`/`, (req, res) => {
  playerController
    .getAllPlayer()
    .then((resultFromController) => res.send(resultFromController));
});

router.post(`/createPlayer`, (req, res) => {
  playerController
    .createPlayer(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.delete(`/deletePlayer/:id`, (req, res) => {
  playerController
    .deletePlayer(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

router.put(`/editPlayer/:id`, (req, res) => {
  playerController
    .updatePlayer(req.params.id, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

router.get(`/:id`, (req, res) => {
  playerController
    .getSpecificPlayer(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});

router.post(`/login`, (req, res) => {
  playerController
    .loginPlayer(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
