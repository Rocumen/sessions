const Player = require("../models/Player");
const bcrypt = require(`bcrypt`);

const auth = require(`../auth`);

module.exports.getAllPlayer = () => {
  return Player.find({}).then((result) => {
    return result;
  });
};

module.exports.createPlayer = (requestBody) => {
  let newPlayer = new Player({
    firstName: requestBody.firstName,
    lastName: requestBody.lastName,
    email: requestBody.email,
    password: bcrypt.hashSync(requestBody.password, 10),
    eSport: [
      {
        ign: requestBody.ign,
        gamingSince: requestBody.gamingSince,
        gameNiche: requestBody.gamingNiche,
      },
    ],
  });
  return newPlayer.save().then((player, error) => {
    if (error) {
      console.log(error);
    } else {
      return player;
    }
  });
};

module.exports.deletePlayer = (playerId) => {
  return Player.findByIdAndDelete(playerId).then((removedPlayer, error) => {
    if (error) {
      console.log(error);
    } else {
      return removedPlayer;
    }
  });
};

module.exports.getSpecificPlayer = (playerId) => {
  return Player.findById(playerId).then((result) => {
    return result;
  });
};

module.exports.updatePlayer = (playerId, requestBody) => {
  return Player.findById(playerId).then((result, error) => {
    if (error) {
      console.log(error);
      return result.send(`Cannot find the data with the provided Id.`);
    }

    result.firstName = requestBody.firstName;

    return result.save().then((updatedPlayer, error) => {
      if (error) {
        console.log(error);
        return res.send(`There is an error while saving the update`);
      } else {
        return updatedPlayer;
      }
    });
  });
};

module.exports.loginPlayer = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    console.log(result);
    if (result === null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      console.log(isPasswordCorrect);

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};
