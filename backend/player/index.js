const express = require(`express`);
const mongoose = require(`mongoose`);
const playerRoute = require(`./routes/playerRoute`);
const cors = require(`cors`);

require(`dotenv`).config();
// Environment Setup
const app = express();
const port = 3000;
// server setup
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(`/player`, playerRoute);
// cors allows all resources to have access on backend app
app.use(cors());

//Database Connection
mongoose.connect(process.env.dbURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

let db = mongoose.connection;

db.once(`open`, () => console.log(`We're connected to the cloud database.`));

// server gateway response

if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
  });
}

module.exports = app;
