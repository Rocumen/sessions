const mongoose = require(`mongoose`);

const playerSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, `First name is required!`],
  },
  lastName: {
    type: String,
    // required: [true, `Last name is required.`],
  },
  email: {
    type: String,
    // required: [true, `Email is required.`],
  },
  password: {
    type: String,
    // required: [true, `Password is required.`],
  },
  eSport: [
    {
      ign: {
        type: String,
        // required: [true, `Please put your ign`],
      },
      gamingSince: {
        type: Date,
        default: new Date(),
      },
      gameNiche: {
        type: String,
      },
      title: {
        type: String,
        default: "Gamer",
      },
    },
  ],
});

module.exports = mongoose.model(`Player`, playerSchema);
