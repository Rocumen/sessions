

let grades = [98, 94, 99, 90];

// not recommended - not a good practice

let mixedArray = [`John`, false, {}, 15];

//

let city1 = `Tokyo`;
let city2 = `Manila`;
let city3 = `Jakarta`;

let cities = [city1, city2, city3]
console.log(cities)

// `length` method

console.log(cities.length);

let blankArray = [];
console.log(blankArray.length);


//length method will count the number of characters including empty space
let fullName = `Jami Noble`;
console.log(fullName.length);

let myTask = [
    `Drink HTML`,
    `eat Javascript`,
    `Inhale CSS`,
    `Bake sass`
];

console.log(myTask);
console.log(myTask.length);

//use `length` Method to delete array value

myTask.length = myTask.length - 1;
console.log(myTask.length);
console.log(myTask);

//another way of deleteing an array value

myTask.length--
console.log(myTask.length);
console.log(myTask);

let theBeatles = [
    `John`,
    `Paul`,
    `Ringo`,
    `George`
];

theBeatles.length++
console.log(theBeatles)

console.log(theBeatles[20]);


//to add value inside array from outside
theBeatles[4] = `test`;
console.log(theBeatles);

theBeatles[0] = `Tubol`;
console.log(theBeatles);

// Accessing the last index

let lakersLegends = [
    `Kobe`,
    `Shaq`,
    `Lebron`,
    `Magic`,
    `Kareem`
]

let lastIndexElement = lakersLegends.length - 1;
console.log(lakersLegends[lastIndexElement])

let addIndexElement = lakersLegends.length;

lakersLegends[addIndexElement] = `test`
console.log(lakersLegends)

//adding elements in an array 
let newArray = [];

newArray[0] = `Cloud Strife`;
newArray[1] = `Tifa Lockhart`;
newArray[newArray.length] = `Wallace`;

console.log(newArray);

newArray.push(`test`) // another way to add
console.log(newArray)

// for loops with array

for (let i = 0; i < newArray.length; i++) {
    console.log(newArray[i]);
}

let numbers = [5, 12, 30, 46, 40];

for (let i = 0; i < numbers.length; i++) {
    if (numbers[i] % 5 === 0) {
        console.log(numbers[i] + `is divisible by 5.`);
    } else {
        console.log(numbers[i] + `is not divisible by 5.`);
    }
}




