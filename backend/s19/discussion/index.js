console.log("Helluw World")

// [Section] syntax, statements and comments
    // js statements usually end with semicolon (;)

    // there are two types of comments:
        // 1. the single line comment denoted by //
        // 2. the multiple line comment denoted by (Shift alt a) /**/,
        // double and asterisk in the middle  /*  */

// [section] variables
    //it is used to contain data
    // any information that is used by an application is stored in what we call a memory"

    // declaring variables

    //declaring variable-tells our devices that a variable name is created.

    let myVariable; // A naming convension
    let myVariableNumber
    let my-variable-number
    let my_variable_number

    //syntax
        // Let/const/var variableName

    //declaring and initializing variables

    let productName = "desktop computer";
    console.log(productName);

    let productPrice1 = "18999";
    let productPrice = 18999;
    console.log(productPrice1);
    console.log(productPrice);

    const interest = 3.539;

    //reassigning variable values

    productName = "Laptop";
    console.log(productName)

    //