//[ SECTION ] Functions
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
    // Functions are mostly created to create complicated tasks to run several lines of code in succession
    // They are also used to prevent repeating lines/blocks of codes that perform the same task/function


//Function declarations
    //(function statement) defines a function with the specified parameters.

    /*
    Syntax:
        function functionName() {
            code block (statement)
        }
    */
    // function keyword - used to defined a javascript functions
    // functionName - the function name. Functions are named to be able to use later in the code.
    // function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.


function printName () {
    console.log ('my name is Reynald')
};

printName ();

 declaredFunction(); //- results in an error, much like variables, we cannot invoke a function we have yet to define.

//[ SECTION ] Functions
    // Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
    // Functions are mostly created to create complicated tasks to run several lines of code in succession
    // They are also used to prevent repeating lines/blocks of codes that perform the same task/function


//Function declarations
    //(function statement) defines a function with the specified parameters.

    /*
    Syntax:
        function functionName() {
            code block (statement)
        }
    */
    // function keyword - used to defined a javascript functions
    // functionName - the function name. Functions are named to be able to use later in the code.
    // function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.
    
    //[ SECTION ] Function Invocation
    //The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
    //It is common to use the term "call a function" instead of "invoke a function".

    function declaredFunction() {
        console.log('hello world from declaredFunction')
    };

    declaredFunction();

    //Function Expression
        //A function can also be stored in a variable. This is called a function expression.

        //A function expression is an anonymous function assigned to the variableFunction

        //Anonymous function - a function without a name.

        //variableFunction();
        /* 
            error - function expressions, being stored 
            in a let or const variable, cannot be hoisted.
        */




    // variableFunction(); - error (Uncaught ReferenceError: Cannot access 'variableFunction' before initialization)

    let variableFunction = function () {
        console.log('hello again')
    }

    variableFunction();

    //[ SECTION ] Function scoping

/*  
    Scope is the accessibility (visibility) of variables.
    
    Javascript Variables has 3 types of scope:
        1. local/block scope
        2. global scope
        3. function scope
            JavaScript has function scope: Each function creates a new scope.
            Variables defined inside a function are not accessible (visible) from outside the function.
            Variables declared with var, let and const are quite similar when declared inside a function
*/  

{
    let localVar = 'Naruto Uzumaki';
}

let globalVar = 'Sasuke Uchiha';

console.log(globalVar);
// console.log(localVar); - error 

function showNames() {
    var functionVar = 'Tony';
    const functionCons = 'Kap';
    let functionLet = 'Thor';

    console.log(functionVar);
    console.log(functionCons);
    console.log(functionLet);

    console.log(globalVar); // global variables can be called inside a local/block scope and function scope
}

showNames();

 //Nested Functions

        //You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.


        function myNewFunction() {
            let name = 'owen;'

            function nestedFunction () {
                let nestedName = 'Moon';
                console.log(name);
                
            }

            // console.log(nestedName);

            nestedFunction();
        }

        myNewFunction();
        // nestedFunction(); error

        function myNewFunction2() {
            myNewFunction();

            console.log('this is new function 2');
        };

        myNewFunction2();

         //[ SECTION ] Using alert()

    //alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.


    // alert('hello batch 305');

    function showSampleAlert() {
        alert('tubol!');
    }

    showSampleAlert();

    console.log('I will only log in the console when the alertis dismissed.');


    /*Notes on using Alert
        *show only an alert() for short dialogs/messages
        *do not overuse alert() because the program has to wait for it to be dismissed.

    */

        //[ SECTION ] Using alert()

    //alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.
  
  //[ SECTION ] Using prompt()
    
    //prompt() allows us to show a small window at the of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.


    // let samplePrompt = prompt('enter your name:');

    // console.log('Hello ' + samplePrompt);

    let sampleNullPrompt = prompt('Do not enter anything');

    console.log(sampleNullPrompt); //prompt() returns an empty string when there is no input. Or null if the user cancels the prompt().

  // [SECTION] RETURN STATEMENT
	// "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
	// "return" statement also stops the execution of the function and any code after the return statement will not be executed.

    function returnFunc(){
        return 'I am a return statement'
    }

    returnFunc(); // no display in the console

    function returnName() {

        return "Owen" + " " + "orange"
        console.log('This message will not be printed');
    };

    let name = returnName();
    console.log(returnName());

    //[ SECTION ] Function Naming Conventions
    //Function names should be definitive of the task it will perform. It usually contains a verb.

    function getCourses() {
        let courses = ['Science 101', 'Math 101', 'English 101'];
        return courses;
    }

    console.log(getCourses());

    //Avoid generic names to avoid confusion within your code. Use definitive names

    function get() {
        let name = 'Noir'
        return 'name'
    }

    console.log(get())

    // [SECTION] RETURN STATEMENT
	// "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
  
  
    //Avoid pointless and inappropriate function names.

    function foo(){

        console.log(25%5);

    };

    foo();

//Name your functions in small caps. Follow camelCase when naming variables and functions.

    function displayCarInfo(){

        console.log("Brand: Toyota");
        console.log("Type: Sedan");
        console.log("Price: 1,500,000");

    }
    
    displayCarInfo();

// "return" statement also stops the execution of the function and any code after the return statement will not be executed.