console.log('hello world')

function printName(pangalan) {
    console.log(`My name is ${pangalan}`)
}

printName('Reynald');
printName('Chupapi');

let sampleVariable = 'John'

printName(sampleVariable);

// check divisibility

function checkDivisibility(num) {
    let remainder = num % 8
    console.log(`the remainder of ${num} divided by 8 is: ${remainder}`);

    let isDivisibleBy8 = remainder === 0;
    console.log(`Is ${num} divisible by 8?`);
    console.log(isDivisibleBy8);
}

checkDivisibility(64);

//function as arguments

function argumentFuntion() {
    console.log('This function was passed as an argument')
}

function invokeFunction(argument) {
    argumentFuntion();
}

invokeFunction();

//multiple Parameters in a Function

function createFullName(firstName, middleName, lastName) {
    console.log(`${firstName} ${middleName} ${lastName}`);
}

createFullName('Reynald', 'Calamanan', 'Ocumen');
createFullName('Reynald', 'Ocumen', 'tubol')

function showSampleAlert() {
    alert('Hello panget');
}

console.log('Testing');

//prompt()

//let samplePrompt = prompt('Enter Your Name')

//console.log('Hello,' + samplePrompt)

function printWelcomeMessage() {
    let firstName = prompt(`Enter your First Name`);
    let lastName = prompt(`Enter you Last Name`);

    console.log(`Hello, ${firstName} ${lastName} !`)
    console.log(`Welcome to my page!`);
}

//printWelcomeMessage();

function greeting() {
    return `Hello, This is the return statement`;
    //code below return statement will never work
    console.log('asdasd')
}

let greetingFunction = greeting()
console.log(greetingFunction)