
/*
1. Create a function called addNum which will be able to add two numbers.
    - Numbers must be provided as arguments.
    - Return the result of the addition.
   
   Create a function called subNum which will be able to subtract two numbers.
    - Numbers must be provided as arguments.
    - Return the result of subtraction.

    Create a new variable called sum.
     - This sum variable should be able to receive and store the result of addNum function.

    Create a new variable called difference.
     - This difference variable should be able to receive and store the result of subNum function.

    Log the value of sum variable in the console.
    Log the value of difference variable in the console.*/




function addNum(num1, num2) {
    let display = `Displayed sum of ${num1} and ${num2}`;
    console.log(display);
    let result = num1 + num2;
    console.log(result);
    return result;


}

addNum(5, 5);


function subNum(num1, num2) {
    let display = `Displayed difference of ${num1} and ${num2}`;
    console.log(display);
    let result = num1 - num2;
    console.log(result);
    return result;
}

subNum(5, 3);

let sum = addNum;


let difference = subNum;





/*
    2. Create a function called multiplyNum which will be able to multiply two numbers.
        - Numbers must be provided as arguments.
        - Return the result of the multiplication.

        Create a function divideNum which will be able to divide two numbers.
        - Numbers must be provided as arguments.
        - Return the result of the division.

        Create a new variable called product.
         - This product variable should be able to receive and store the result of multiplyNum function.

        Create a new variable called quotient.
         - This quotient variable should be able to receive and store the result of divideNum function.

        Log the value of product variable in the console.
        Log the value of quotient variable in the console.*/

function multiplyNum(num3, num4) {
    let display = `The product of ${num3} and ${num4}`;
    console.log(display);
    let result = num3 * num4;
    console.log(result);
    return result;
}

multiplyNum(2, 5);

function divideNum(num6, num7) {
    let display = `The quotient of ${num6} and ${num7}`;
    console.log(display);
    let result = num6 / num7;
    console.log(result);
    return result;
}

divideNum(10, 5);

let product = multiplyNum;

let quotient = divideNum;



/*
    3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
        - a number should be provided as an argument.
        - look up the formula for calculating the area of a circle with a provided/given radius.
        - look up the use of the exponent operator.
        - return the result of the area calculation.

        Create a new variable called circleArea.
        - This variable should be able to receive and store the result of the circle area calculation.
        - Log the value of the circleArea variable in the console.*/

function getCircleArea(num1) {
    let circleArea = 3.1416 * (num1 * num1);
    console.log(`The result of getting the area of a circle with a ${num1} radius:`);
    console.log(circleArea);
    return circleArea;
}

getCircleArea(15);
let circleArea = getCircleArea;




/* 4. Create a function called getAverage which will be able to get total average of four numbers.
     - 4 numbers should be provided as an argument.
     - look up the formula for calculating the average of numbers.
     - return the result of the average calculation.
     
     Create a new variable called averageVar.
     - This variable should be able to receive and store the result of the average calculation
     - Log the value of the averageVar variable in the console.*/

function getAverage(num1, num2, num3, num4) {
    let result = (num1 + num2 + num3 + num4) / 4;
    console.log(`The result of ${num1},${num2},${num3},${num4} `);
    console.log(result);
    return result;
}

getAverage(2, 4, 6, 8);
let averageVar = getAverage;


/* 5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
     - this function should take 2 numbers as an argument, your score and the total score.
     - First, get the percentage of your score against the total. You can look up the formula to get percentage.
     - Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
     - return the value of the variable isPassed.
     - This function should return a boolean.

     Create a global variable called outside of the function called isPassingScore.
         -This variable should be able to receive and store the boolean result of the checker function.
         -Log the value of the isPassingScore variable in the console.
*/

function checkIfPassed(grade1, grade2) {
    let average = (grade1 / grade2) * 100;
    let passingPercentage = (75 / 100) * 100;
    let isPassed = average > passingPercentage;
    console.log(isPassed);
    return isPassed

}

checkIfPassed(80, 100);
let isPassingScore = checkIfPassed;
console.log(isPassingScore);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
    module.exports = {

        addNum: typeof addNum !== 'undefined' ? addNum : null,
        subNum: typeof subNum !== 'undefined' ? subNum : null,
        multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
        divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
        getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
        getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
        checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

    }
} catch (err) {

}