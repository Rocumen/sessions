// [SECTION ] Dependecies and Modules
const express = require(`express`);
const courseController = require(`../controllers/course`);
const auth = require(`../auth.js`);

// [SECTION] Routing Component
const router = express.Router();

// Destructure from auth
const { verify, verifyAdmin } = auth;

router.post(`/`, verify, verifyAdmin, courseController.addCourse);

// Get all courses
router.get(`/all`, courseController.getAllCourses);

// Get all Active course
router.get(`/`, courseController.getAllActive);

//delete one course
router.delete(`/deleteCourse`, courseController.deleteCourse);

// Get a specific course using its ID
router.get(`/:courseId`, courseController.getCourse);

// Updating a Course (Admin Only)
router.put(`/:courseId`, verify, verifyAdmin, courseController.updateCourse);

//archive course
router.put(
  `/:courseId/archive`,
  verify,
  verifyAdmin,
  courseController.archiveCourse
);

// activating course
router.put(
  `/:courseId/activate`,
  verify,
  verifyAdmin,
  courseController.activateCourse
);

module.exports = router;
