// [SECTION ] Dependecies and Modules
const express = require(`express`);
const userController = require(`../controllers/user`);
const auth = require(`../auth.js`);

// [SECTION] Routing Component
const router = express.Router();

// Destructure from auth
const { verify, verifyAdmin } = auth;

//get User info

router.get(`/allUser`, (req, res) => {
  userController
    .getAllUser()
    .then((resultFromController) => res.send(resultFromController));
});

// check email routes
router.post(`/checkEmail`, (req, res) => {
  userController
    .checkEmailExist(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// user registration routes
router.post(`/register`, (req, res) => {
  userController.registerUser(req.body).then((resultFromController) => {
    res.send(resultFromController);
  });
});

// User authentication
router.post(`/login`, (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// retrieve user details
// router.post("/details", verify, (req, res) => {
//   userController
//     .getProfile(req.body)
//     .then((resultFromController) => res.send(resultFromController));
// });

router.get(`/details`, verify, userController.getProfile);

// enroll user to a course
router.post(`/enroll`, verify, userController.enroll);

//router.post(`/login`, userController.loginUser);

// get enrollments
router.get(`/getEnrollments`, verify, userController.getEnrollments);

// Reset password
router.put(`/reset-password`, verify, userController.resetPassword);

// [SECTION] Export Route System
module.exports = router;
