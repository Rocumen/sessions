const bcrypt = require(`bcrypt`);
const auth = require(`../auth`);
const Course = require("../models/Course");
const User = require("../models/User");

module.exports.addCourse = (req, res) => {
  const newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newCourse
    .save()
    .then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })

    .catch((err) => res.send(err));
};

//find all courses
module.exports.getAllCourses = (req, res) => {
  return Course.find({})
    .then((result) => {
      if (result.length === 0) {
        return res.send(`No Data`);
      } else {
        return res.send(result);
      }
    })
    .catch((error) => error);
};

// get all active courses
module.exports.getAllActive = (req, res) => {
  return Course.find({ isActive: true }).then((result) => {
    if (result.length === 0) {
      return res.send(`there is currently no active courses`);
    } else {
      return res.send(result);
    }
  });
};

// Delete one course
module.exports.deleteCourse = (req, res) => {
  return Course.findByIdAndDelete(req.body.id).then((result) => {
    return res.send(result);
  });
};

// Get specific Course
module.exports.getCourse = (req, res) => {
  return Course.findById(req.params.courseId)
    .then((result) => {
      if (result === 0) {
        return res.send(`Cannot find course with the provided ID`);
      } else {
        if (result.isActive === false) {
          return res.send(
            `the course you are tring to access is not available`
          );
        } else {
          return res.send(result);
        }
      }
    })
    .catch((error) => res.send(`Please enter a correct course ID`));
};

// updating Course
module.exports.updateCourse = (req, res) => {
  let updateCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };

  return Course.findByIdAndUpdate(req.params.courseId, updateCourse)
    .then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((error) => res.send(error));
};
//archive course
module.exports.archiveCourse = (req, res) => {
  let archiveCourse = {
    isActive: false,
  };

  return Course.findByIdAndUpdate(req.params.courseId, archiveCourse)
    .then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((error) => res.send(error));
};

// activate course
module.exports.activateCourse = (req, res) => {
  let archiveCourse = {
    isActive: false,
  };

  return Course.findByIdAndUpdate(req.params.courseId, archiveCourse)
    .then((error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    })
    .catch((error) => res.send(error));
};

// get enrollments
