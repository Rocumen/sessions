// exponent operator

//old structure
const firstNum = 8 ** 2;
console.log(firstNum);

//updated structure
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals

let name = `John`;
console.log(`My name is ${name}`);

let anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;

console.log(anotherMessage);

const interestRate = 0.1;

const principal = 1000;
console.log(
  `The interest on your savings account is: ${principal * interestRate}`
);

// Array Destructuring

let fullName = [`Juan`, `Dela`, `Cruz`];

const [firstName, middleName, lastName] = fullName;

console.log(
  `Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`
);

//object destructuring

const person = {
  givenName: `Jane`,
  maidenName: `Dela`,
  familyName: `Cruz`,
};

//destructuring an object

const { givenName, maidenName, familyName } = person;

console.log(`${givenName} ${maidenName} ${familyName}`);

// Arrow Function

const hello = () => {
  console.log(`Hello World!`);
};

const printFullName = (fName, mName, lName) => {
  console.log(`${fName} ${mName} ${lName}`);
};

printFullName(`Reynald`, `Big`, `Smith`);

// Arrow Function with Loops

let students = [`John`, `Jane`, `Judy`];

students.forEach((student) => {
  console.log(`${student} is a student.`);
});

// implicit return statement

const add = (x, y) => {
  return x + y;
};

let total = add(1, 2);
console.log(total);

// implicit return

const sum = (x, y) => x + y;

let total2 = sum(5, 10);

console.log(total2);

// class- based objects blueprints

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}

// instance = duplicate = copy

const myCar = new Car();

console.log(myCar);

myCar.brand = `Ford`;
myCar.name = `Ranger Raptor`;
myCar.year = `2021`;

console.log(myCar);

const myNewCar = new Car(`Toyota`, `Vios`, `2021`);
console.log(myNewCar);
