let http = require(`http`);

let port = 4000;

let app = http.createServer(function (request, response) {
  // GET method
  if (request.url == `/items` && request.method == `GET`) {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end(`Data retreived from the database`);
  }

  // POST method
  if (request.url == `/items` && request.method == `POST`) {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end(`Data to be sent to the database`);
  }
});

//inform us if the server is running, by printing this
app.listen(port, () => console.log(`Server is running at localhost:${port}`));
