// JavaScript Object Notation (JSON)

/*
{
    "city" : "Quezon City",
    "province" : "Metro Manila",
    "country" : "Philippines"
}
*/

// JSON Arrays

/*
"cities" : [
    {"city": "Quezon City", "province" : "Metro Manila", "country" : "Philippines"},
    {"city": "Manila City", "province" : "Metro Manila", "country" : "Philippines"},
    {"city": "Makati City", "province" : "Metro Manila", "country" : "Philippines"}
]
*/

// JSON Methods

let batchesArray = [{ batchName: "Batch X" }, { batchName: "Batch Y" }];

//Stringify method = it makes the object to string

console.log(`Result from stringify:`);
console.log(JSON.stringify(batchesArray));

let data = JSON.stringify({
  name: `Diograt`,
  age: 25,
  address: {
    city: `Manila`,
    country: `Philippines`,
  },
});

console.log(data);

// Using stringify method with variables
/*
let firstName = prompt(`What is you first name?`);
let lastName = prompt(`What is you first last name?`);
let age = prompt(`what is your age?`);
let address = {
  city: prompt(`Which city do you live in?`),
  country: prompt(`Which country does your city address belong to?`),
};

let otherData = JSON.stringify({
  firstName: firstName,
  lastName: lastName,
  age: age,
  address: address,
});

console.log(otherData);
*/
// Convert stringified JSON into JS Objects
//Parse()

let batchesJSON = `[{"batchName" : "Batch X"}, {"batchName" : "Batch Y"} ]`;

console.log(`Result from parse`);
console.log(JSON.parse(batchesJSON));
