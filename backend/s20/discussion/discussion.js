// console.log('hello world');

//

let x = 26;
let y = 5;

let sum = x + y;
console.log('Result from addition operator: ' + sum);

let difference = x - y;
console.log('Result from subtraction operator: ' + difference);

let product = x * y;
console.log('Result from multiplication operator: ' +product)

let quotient = x / y;
console.log('Result from multiplication operator; ' + quotient)

//modulo %
let remainder = x % y;
console.log('Result from modulo operator; ' + remainder)

// [SECTION] Assignment Operator
// Basic Assignment Operator (=)

let assignment_number = 8;

assignment_number = assignment_number + 2;
console.log('addition assignment:' + assignment_number);

// Shorthand Method for Assignment Method

assignment_number += 2;
console.log('addition assignment: ' + assignment_number);

assignment_number -= 2;
console.log('subtraction assignment: ' + assignment_number);

assignment_number *= 2;
console.log('multiplication assignment: ' + assignment_number);

assignment_number /= 2;
console.log('division assignment: ' + assignment_number);

// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5;
console.log('mdas: ' + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log('pemdas: ' + pemdas);

// increment and decrement
// incrementation -> ++
// decrementation -> --

let z = 1; 

// pre-increment
let increment = ++z;
console.log('pre-increment: ' + increment);
console.log('pre-increment: ' + z);

//post-increment
increment = z++;
console.log('post-increment: ' + increment);

let my_num1 = 1;
let my_num2 = 1;

let pre_increment = ++my_num1;
pre_increment = ++my_num1;
console.log(pre_increment);

let post_increment = my_num2++;
post_increment = my_num2++;
console.log(post_increment);

let my_num_a = 5;
let my_num_b = 5;

let pre_decrement = --my_num_a;
pre_decrement = --my_num_a;
console.log(pre_decrement);

let post_decrement = my_num_b--;
post_decrement= my_num_b--;
console.log(post_decrement);

// [section] type coercion
// automatic or implicit conversion of value from one data type to another.

let my_num3 = '10';
let my_num4 = 12;

let coercion = my_num3 + my_num4;
console.log(coercion);
console.log(typeof coercion);

let my_num_5 = true + 1;
console.log(my_num_5)

let my_num_6 = false + 1;
console.log(my_num_6);

// [SECTION] Comparison Operator


// Equality operator (==)
let juan = 'juan';
console.log(juan == 'juan');
console.log(1 == 2);
console.log(false == 0)

// Inequality Operator (!=)
// ! -> NOT

console.log(juan != 'juan'); //false
console.log(1 != 2); //true
console.log(false != 0) // false

// Strict Equality Operator

console.log(1 === 1); // true
console.log(false === 0) // false coz hindi pareho ng datatype

console.log(1 !== 1); //false
console.log(false !== 0) // true

// [section] Relational Operator
//comparison operators whether one value is greater or less than to the other value

let a = 50;
let b = 50;

let example1 = a > b;
console.log(example1);

let example2 = a < b;
console.log(example2);

let example3 = a >= b;
console.log(example3);

let example4 = a >= b;
console.log(example4);

// [SECTION] Logical Operator

// logical 'AND' (&& - Double Ampersand)

let is_legal_age = true;
let is_registered = true;

// all Conditions should be true
let all_requirements_met = is_legal_age && is_registered;
console.log('Logical AND: ' + all_requirements_met);

//logical 'OR' (|| - Double Pipe)

let is_legal_age2 = true;
let is_registered2 = false;

let some_requirements_met = is_legal_age2 || is_registered2
console.log('Logical OR: ' + some_requirements_met)

//Logical 'NOT' (!)

let is_registered3 = false

let some_requirements_not_met = !is_registered3;
console.log('Result from logical NOT: ' + some_requirements_not_met)