import AppNavbar from "./components/AppNavbar";
import { Container } from "react-bootstrap";
import { useEffect, useState } from "react";

import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import CourseView from "./pages/CourseView";
import AdminView from "./pages/AdminView";
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
// Bootstrap

/* BrowserRouter, Routes, Route works together to setup components endpoints  */
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

import "./App.css";
import Logout from "./pages/Logout";
import Error from "./components/Error";

import { UserProvider } from "./UserContext";

function App() {
  const [user, setUser] = useState({
    // token: localStorage.getItem("token")
    id: null,
    isAdmin: null,
  });

  //function for clearing local storage
  const unsetUser = () => {
    localStorage.clear();
  };

  // Check if user info is properly stored upon login and the localStorage if cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);
  return (
    // The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/register" element={<Register />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/adminView" element={<AdminView />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/*" element={<Error />} />
            <Route path="/profile" element={<Profile />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
