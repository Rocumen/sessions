import { Card, Button } from "react-bootstrap";
import PropTypes from "prop-types";
import { useState } from "react";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  const { name, description, price, _id } = courseProp;

  // const [count, setCount] = useState(0);
  // const maxCount = 30;

  // function enroll() {
  //   if (count < maxCount) {
  //     setCount(count + 1);
  //     console.log("Enrollees: " + count);
  //   } else {
  //     window.alert("No more seats");
  //   }
  // }

  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PhP {price}</Card.Text>
        <Link variant="primary" to={`/courses/${_id}`}>
          <Button>Details</Button>
        </Link>
      </Card.Body>
    </Card>
  );
}

//Check if the CourseCard Component is getting the correct prop types
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};

/*
export default function CourseCard() {
  // useState to manage the active function or tab
  const [activeContent, setActiveContent] = useState(`first`);

  // Function to handle Nav.Link click and update active content
  const navClick = (activeTab) => {
    setActiveContent(activeTab);
  };

  return (
    <Row>
      <Col>
        <Card className="CourseCard mt-2">
          <Card.Header>
            <Nav
              variant="tabs"
              defaultActiveKey="#first"
              activeKey={`#${activeContent}`}
            >
              <Nav.Item>
                <Nav.Link
                  href="#first"
                  activeTab="first"
                  onClick={() => navClick("first")}
                >
                  Frontend
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="#second"
                  activeTab="second"
                  onClick={() => navClick("second")}
                >
                  Backend
                </Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link
                  href="#third"
                  activeTab="third"
                  onClick={() => navClick("third")}
                >
                  Full Stack
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Card.Header>
          <Card.Body>
            <Card.Title>Sample course</Card.Title>
            {activeContent === "first" && (
              <Card.Text>
                Description:
                <p>This is a sample frontend course offering</p>
              </Card.Text>
            )}
            {activeContent === "second" && (
              <Card.Text>
                Description:
                <p>This is a sample backend course offering</p>
              </Card.Text>
            )}
            {activeContent === "third" && (
              <Card.Text>
                Description:
                <p>This is a sample full stack course offering</p>
              </Card.Text>
            )}
            <Card.Text>
              Price:
              <p>PHP 40,000</p>
            </Card.Text>
            <Button variant="primary">Enroll</Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
*/
