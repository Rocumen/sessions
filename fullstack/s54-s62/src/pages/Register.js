import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Register() {
  const { user, setUser } = useContext(UserContext);
  const userContext = useContext(UserContext);

  //State hooks to store values of the input fields
  // Syntax: [stateVariable, setterFunction] = useState("")
  // Note: setterFunction is to update the state value
  const [firstName, setFirstName] = useState("");

  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [isActive, setIsActive] = useState();

  console.log(firstName);
  console.log(lastName);
  console.log(email);
  console.log(mobileNo);
  console.log(password);
  console.log(confirmPassword);
  console.log(isActive);

  const registerUser = function (e) {
    // Prevents the default behavior during submission which is page redirection via form submission
    e.preventDefault();

    fetch("http://localhost:4000/users/register ", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data) {
          setFirstName("");
          setLastName(``);
          setEmail(``);
          setMobileNo(``);
          setPassword(``);
          setConfirmPassword(``);

          alert(`Thank you for registering`);
        } else {
          alert(`Please try again later`);
        }
      });
  };

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobileNo !== "" &&
      (password !== "") & (confirmPassword !== "") &&
      password === confirmPassword &&
      mobileNo.length === 11
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <h1 className="my-5 text-center">Register</h1>
      <Form.Group>
        <Form.Label>First Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter First Name"
          required
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Last Name:</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Last Name"
          required
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Email:</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          required
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Mobile No:</Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter 11 Digit No."
          required
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter Password"
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Confirm Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Confirm Password"
          required
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
      </Form.Group>
      {isActive ? (
        <Button variant="primary" type="submit">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
