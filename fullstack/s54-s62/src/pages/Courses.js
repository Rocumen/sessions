import { useContext, useEffect, useState } from "react";
import CourseCard from "../components/CourseCard";
import AdminView from "./AdminView";
import UserContext from "../UserContext";

export default function Courses() {
  // console.log(coursesData); // to check if mock data has been captured
  // console.log(coursesData[0]);

  const { user } = useContext(UserContext);

  const [courses, setCourses] = useState([]);

  useEffect(() => {
    // get all active courses
    //http://localhost:4000/courses/
    fetch(`${process.env.REACT_APP_API_URL}/courses/`)
      .then((res) => res.json())
      .then((data) => {
        //mapping creates an array
        if (user.isAdmin) {
          setCourses(data);
        } else {
          setCourses(
            data.map((course) => {
              return <CourseCard key={course._id} courseProp={course} />;
            })
          );
        }
      }, []);
  });
  //mapping creates an array
  // const courses = coursesData.map((course) => {
  //   return <CourseCard key={course.id} courseProp={course} />;
  // });

  return user.isAdmin ? (
    <>
      <AdminView coursesData={courses} />{" "}
    </>
  ) : (
    <>{courses}</>
  );
}
