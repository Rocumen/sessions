import { Navigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import UserContext from "../UserContext";

export default function Logout() {
  localStorage.clear();

  const { unsetUser, setUser } = useContext(UserContext);

  // updates localStorage to empty / clears the storage
  unsetUser();

  useEffect(() => {
    // setter function from App.js
    // sets user state saved in context to null
    setUser({ id: null, isAdmin: null });
  });

  // Redirect back to login
  return <Navigate to="/login" />;
}
