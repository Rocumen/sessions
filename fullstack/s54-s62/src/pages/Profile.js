import React, { useContext, useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

export default function Profile() {
  const [userData, setUserData] = useState(null); // State to store user data
  const { user } = useContext(UserContext);

  useEffect(() => {
    // Fetch user data when the component mounts
    fetch("http://localhost:4000/users/details", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`, // Use Authorization header for JWT token
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the fetched user data in the state
        setUserData(data);
      })
      .catch((error) => {
        console.error("Error fetching user data:", error);
      });
  }, []); // Empty dependency array to ensure it runs only once when the component mounts

  return user.access === null ? (
    <Navigate to="/register" />
  ) : (
    <>
      <Row>
        <h1>User Profile</h1>
        {userData ? (
          <div>
            <p>Email: {userData.email}</p>
            <p>First Name: {userData.firstName}</p>
            <p>Last Name: {userData.lastName}</p>
            <p>Mobile Number: {userData.mobileNo}</p>
            {/* Add other user details here */}
          </div>
        ) : (
          <p>Loading user data...</p>
        )}
      </Row>
    </>
  );
}
