const coursesData = [
  {
    id: `wdc001`,
    name: `PHP - Laravel`,
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,`,
    price: 45000,
    onOffer: true,
  },
  {
    id: `wdc002`,
    name: `Python - Django`,
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,`,
    price: 50000,
    onOffer: true,
  },
  {
    id: `wdc003`,
    name: `Java - Springboot`,
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting,`,
    price: 55000,
    onOffer: true,
  },
];

export default coursesData;
