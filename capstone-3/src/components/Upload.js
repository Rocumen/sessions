import axios from "axios";
import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { ThreeDots } from "react-loader-spinner";

function Upload() {
  const [img, setImg] = useState(null);
  const [video, setVideo] = useState(null);
  const [loading, setLoading] = useState(false);

  const uploadFile = async (type) => {
    const data = new FormData();
    data.append("file", type === "image" ? img : video);
    data.append(
      "upload_preset",
      type === "image" ? "images_preset" : "videos_preset"
    );

    try {
      let cloudName = process.env.REACT_APP_CLOUDINARY_CLOUD_NAME;
      let resourceType = type === "image" ? `image` : `video`;
      let api = `https://api.cloudinary.com/v1_1/${cloudName}/${resourceType}/:upload`;

      const res = await axios.post(api, data);
      const { secure_url } = res.data;
      console.log(secure_url);
      return secure_url;
    } catch (error) {
      console.log(error);
    }
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);
      const imgUrl = await uploadFile("image");

      const videoUrl = await uploadFile("video");

      //   await axios.post(`${process.env.REACT_APP_BACKEND_BASEURL}/api/videos`, {
      //     imgUrl,
      //     videoUrl,
      //   });

      setImg(null);
      setVideo(null);

      console.log(`file upload success`);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <>
      <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Form.Label htmlFor="video">Video</Form.Label>
          <br />
          <input
            type="file"
            accept="video/"
            id="video"
            onChange={(e) => setVideo((prev) => e.target.files[0])}
          />
        </Form.Group>

        <Form.Group>
          <Form.Label htmlFor="image">Image</Form.Label>
          <br />
          <input
            type="file"
            accept="image/"
            id="image"
            onChange={(e) => setImg((prev) => e.target.files[0])}
          />
        </Form.Group>
        <Button type="submit">Upload</Button>
      </Form>

      {loading && (
        <ThreeDots
          height="80"
          width="80"
          radius="9"
          color="#4fa94d"
          ariaLabel="three-dots-loading"
          wrapperStyle={{}}
          wrapperClassName=""
          visible={true}
        />
      )}
    </>
  );
}

export default Upload;
