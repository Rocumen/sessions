import React from "react";
import { Container, Card, Button, Form } from "react-bootstrap";
import QuantityUpdater from "./QuantityUpdater";

export default function CartView({
  cartItems,
  userName,
  updateQuantity,
  removeFromCart,
  checkOut,
  selectedProducts,
  handleChange,
}) {
  return (
    <Container className="mt-5">
      <h2>Your Cart, {userName}</h2>
      {cartItems && cartItems.length > 0 ? (
        cartItems.map((item, index) => (
          <Card key={index} className="mb-3">
            <Card.Body>
              <Form.Check
                type="checkbox"
                id={`productCheckbox${index}`}
                checked={selectedProducts.includes(item.productName)}
                onChange={() => handleChange(item.productName)}
              />
              <Card.Title>{item.productName}</Card.Title>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              {/* QuantityUpdater Component */}
              <QuantityUpdater
                productName={item.productName}
                initialQuantity={item.quantity}
                onUpdate={updateQuantity}
              />
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {item.price}</Card.Text>
              <Card.Subtitle>Total Price:</Card.Subtitle>
              <Card.Text>PhP {item.totalPrice}</Card.Text>
              <Button
                variant="danger"
                onClick={() => removeFromCart(item.productName)} // Call removeFromCart with productId
              >
                Remove from Cart
              </Button>
              <Button
                variant="primary"
                onClick={() => checkOut(item.productName, item.quantity)} // Call removeFromCart with productId
              >
                Check out!
              </Button>
            </Card.Body>
          </Card>
        ))
      ) : (
        <p>Your cart is empty.</p>
      )}
    </Container>
  );
}
