import { Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

// data is received through parameter
export default function Banner({ data }) {
  // Object destructuring
  const { title, content, destination, label } = data;

  return (
    <div className="welcum row z-1 text-white">
      <div className="p-5 text-center position-relative col mother">
        <div className="position-absolute content">
          <h1>{title}</h1>
          <p>{content}</p>
          <Button>
            <Link className="redirect" to={destination}>
              {label}
            </Link>
          </Button>
        </div>
      </div>
    </div>
  );
}
