import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

import images from "../images/headers.webp";
import ps from "../images/ps-network-blue-bag-icon-01-01feb23.webp";

export default function Home() {
  const data = {
    /* textual contents*/
    title: "Welcome to Playstation Network",
    content:
      "Online gaming, entertainment, friends, shopping and more - PSN is where your online journey begins.",
    /* buttons */
    destination: "/products",
    label: "Shop now!",
  };

  return (
    <>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <img src={images} className="headers " alt="header" />
            <img src={ps} alt="ps" />
            <div>
              <Banner className="welcum" data={data} />
            </div>
          </div>
        </div>

        {/* <Highlights /> */}
      </div>
    </>
  );
}
